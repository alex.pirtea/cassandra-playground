## Short description:  
Cassandra playground with monitoring configured

## What you need:
* helm v3
* kubectl
* local kubernetes deployment 
  * I use docker client to bootstrap one


## Get started
1. Install [Prometheus Operator](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)

    ```bash
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts ;
    helm repo update ;
    helm upgrade --install --atomic -f prometheus.yaml prometheus prometheus-community/kube-prometheus-stack ;
    ```
    > Note: helm install example assumes you're running from within this repository path. If you need to run from another path update the path of prometheus.yaml
1. Install [Cassandra](https://github.com/bitnami/charts/tree/master/bitnami/cassandra)
    ```bash
    helm repo add bitnami https://charts.bitnami.com/bitnami ;
    helm repo update ;
    helm upgrade --install --atomic -f cassandra.yaml cassandra  bitnami/cassandra ;
    ```

1. Import [graphana dashbaoard](https://grafana.com/grafana/dashboards/10849)
    ```bash
    kubectl port-forward svc/prometheus-grafana 9001:80
    ```
    > Note: Default graphana credentials  
    > username: admin   
    > password: prom-operator

1. (Optional) Install an [ingress controller](https://kubernetes.github.io/ingress-nginx/deploy/#docker-desktop) 
    ```
    # docker desktop example
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.0/deploy/static/provider/cloud/deploy.yaml
    ```

1. (Optional) Apply ingress configuration
    ```bash
    kubectl apply -f ingress.yaml
    echo 127.0.0.1 prometheus.local | sudo tee -a /etc/hosts
    echo 127.0.0.1 cassandra.local | sudo tee -a /etc/hosts
    ```

1. (Optional) Install kubernetes metrics server for commands like `kubectl top pods`
    ```bash
    kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/download/v0.4.2/components.yaml
    kubectl patch deployment metrics-server -n kube-system --type 'json' -p '[{"op": "add", "path": "/spec/template/spec/containers/0/args/-", "value": "--kubelet-insecure-tls"}]'
    ```
